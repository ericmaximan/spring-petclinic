FROM gradle:7.1.0-jdk11 AS build
COPY --chown=gradle:gradle . /home/gradle/src
WORKDIR /home/gradle/src
RUN gradle build --no-daemon

FROM openjdk:8-jre-slim

EXPOSE 8080

RUN mkdir /app

COPY --from=build /home/gradle/src/build/libs/*.jar /app/

ENTRYPOINT ["java", "-XX:+UnlockExperimentalVMOptions", "-XX:+UseCGroupMemoryLimitForHeap", "-Djava.security.egd=file:/dev/./urandom","-jar","/app/spring-boot-application.jar"]
#
#FROM openjdk:13-alpine as builder
#WORKDIR application
#ARG ARTIFACT_NAME
#COPY ${ARTIFACT_NAME}.jar application.jar
#RUN java -Djarmode=layertools -jar application.jar extract
#
## Download dockerize and cache that layer
#ARG DOCKERIZE_VERSION
#RUN wget -O dockerize.tar.gz https://github.com/jwilder/dockerize/releases/download/${DOCKERIZE_VERSION}/dockerize-alpine-linux-amd64-${DOCKERIZE_VERSION}.tar.gz
#RUN tar xzf dockerize.tar.gz
#RUN chmod +x dockerize
#
#
## wget is not installed on adoptopenjdk:11-jre-hotspot
#FROM adoptopenjdk:11-jre-hotspot
#
#WORKDIR application
#
## Dockerize
#COPY --from=builder application/dockerize ./
#
#ARG EXPOSED_PORT
#EXPOSE ${EXPOSED_PORT}
#
#ENV SPRING_PROFILES_ACTIVE docker
#
#COPY --from=builder application/dependencies/ ./
#COPY --from=builder application/spring-boot-loader/ ./
#COPY --from=builder application/snapshot-dependencies/ ./
#COPY --from=builder application/application/ ./
#ENTRYPOINT ["java", "org.springframework.boot.loader.JarLauncher"]
